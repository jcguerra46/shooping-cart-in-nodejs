const multer = require('multer')
const path = require('path')

var segment = '';

var storage = multer.diskStorage({
    destination: (req, file, cb) => {

        if(req.baseUrl == '/users') {
            segment = 'avatars';
        } else if (req.baseUrl == '/products') {
            segment = 'products';
        } else {
            segment = 'others';
        }

        cb(null, path.join(__dirname, `../../public/uploads/${segment}`));
    },
    filename: (req, file, cb) => {
      cb(null, req.body.email + Date.now() + path.extname(file.originalname));
    }
})
   
var upload = multer({ 
    storage: storage,
    fileFilter(req, file, cb){
        cb(null, file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif');
    }
 })

module.exports = upload