const express = require('express');
const router = express.Router();

// App Require files
const homeRouter = require('./homeRoutes');
const loginRouter = require('./loginRoutes');
const logoutRouter = require('./logoutRoutes');
const productRouter = require('./productRoutes');
const userRouter = require('./userRoutes');

// API Require files
const userApiRouter = require('./api/userApiRoutes');
const productApiRouter = require('./api/productApiRoutes');

// App routes
router.use('/', homeRouter);
router.use('/login', loginRouter);
router.use('/logout', logoutRouter);
router.use('/products', productRouter);
router.use('/users', userRouter);

// API routes
router.use('/api/users', userApiRouter);
router.use('/api/products', productApiRouter);

module.exports = router;